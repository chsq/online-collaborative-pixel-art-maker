GRID_WIDTH = 32;
GRID_HEIGHT = 32;
let grid;
let cs;
let rSlider;
let gSlider;
let bSlider;
let colorw;

let sock = io.connect('http:/...:3000');
sock.on('connect', function () {
  console.log('connection created...');
});






function setup(){
  createCanvas(512,512);
  drawGrid = createCheckbox('draw grid', true);
  cell_width = width/GRID_WIDTH;
  cell_height = height/GRID_HEIGHT;
  noLoop();
  sock.on("gridload",function(data){
    grid = data;
  });

  sock.on('update',function(data){
    grid[data.y][data.x][0] = data.r;
    grid[data.y][data.x][1] = data.g;
    grid[data.y][data.x][2] = data.b;
    loop();
  })

  sock.emit('getgrid');
  cs = new color_selector();
  rSlider = createSlider(0, 255, 0);
  gSlider = createSlider(0, 255, 0);
  bSlider = createSlider(0, 255, 0);
  colorw = createElement('h2', 'color');
}

function draw(){

  if(drawGrid.checked()){
    stroke(0);
  }else{
    noStroke();
  }
  colorw.style("color","rgb("+cs.r+","+cs.g+","+cs.b+")")
  cs.update();
  for(row in grid){
    for(col in grid[row]){
      fill(grid[row][col][0],grid[row][col][1],grid[row][col][2]);
      rect(col*cell_width,row*cell_height,cell_width,cell_height);
    }
  }
  if(mouseIsPressed){
    gx = Math.floor(mouseX / cell_width);
    gy = Math.floor(mouseY / cell_height);
    set_color(gx,gy);
  }
}

function create_grid(w,h){
  let grid = [];
  for(i = 0;i < h;i++){
    grid[i] = [];
    for(j = 0;j < w;j++){
      grid[i][j] = [255,255,255];
    }
  }
  return grid;
}

function color_selector(){
  this.r = 0;
  this.g = 0;
  this.b = 0;
  this.update = function(){
    this.r = rSlider.value();
    this.g = gSlider.value();
    this.b = bSlider.value();
    loop();
  }
}

function set_color(gx,gy){
  if(gy < GRID_HEIGHT && gx < GRID_WIDTH && gy >= 0 && gx >= 0){
      //grid[gy][gx] = [cs.r,cs.g,cs.b];
      sock.emit('put',{y:gy,x:gx,r:cs.r,g:cs.g,b:cs.b});
  }
}
