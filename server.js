GRID_WIDTH = 32;
GRID_HEIGHT = 32;

var app = require('http').createServer(handler);
var io = require('socket.io')(app);

var grid = create_grid(GRID_WIDTH,GRID_HEIGHT);

io.set('origins', '*:*');
app.listen(3000);
io.listen(app);



io.on('connection', function (socket){

  socket.on('getgrid', function(){
    socket.emit('gridload',grid)
  });

  socket.on('put', function(data){
    grid[data.y][data.x][0] = data.r;
    grid[data.y][data.x][1] = data.g;
    grid[data.y][data.x][2] = data.b;
    io.emit('update',data);
  });
});


function create_grid(w,h){
  let grid = [];
  for(i = 0;i < h;i++){
    grid[i] = [];
    for(j = 0;j < w;j++){
      grid[i][j] = [255,255,255];
    }
  }
  return grid;
}

function handler (req, res) {

}
